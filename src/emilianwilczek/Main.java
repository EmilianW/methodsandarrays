/** Emilian Wilczek
 * UHI 16013977
 * Tutorial 1
 * 08/09/2020
 * Full working version. Could be refactored and streamlined.
 */

package emilianwilczek;

import java.util.Scanner;

public class Main {

    public static Scanner keyboard = new Scanner(System.in);

    public static int getRandomNumber()
    {
        return (int)(Math.random()*10) + 1;
    }

    public static int getHouseNumber()
    {
        return (int)(Math.random()*4) + 16;
    }

    public static int calcTotal(int x, int y)
    {
        return x + y;
    }

    public static boolean getResult(int z)
    {
        return z >= 19 && z <= 21;
    }

    public static boolean busted(int z)
    {
        return z > 21;
    }

    public static double percentageOfGamesWon(int[][] card)
    {
        int playedGames = 0;
        int wonGames = 0;

        for (int i = 0; i < 5; i++)
        {
            if (card[i][0] > 0)
            {
                playedGames++;
            }
            int total = 0;
            for (int j = 0; j < 26; j++)
            {
                if (card[i][j] > 0)
                {
                    total = total + card[i][j];
                    if (total >= 19 && total <= 21)
                    {
                        wonGames++;
                    }
                }
            }
        }

        return (double)wonGames / (double)playedGames * 100;
    }

    public static void main(String[] args) {

        int[] drawnCards = new int[10];

        String choice;

        int[][] card = new int[5][26];
        int total;
        int houseTotal;

        String playAgain = "y";


        for (int k = 0; k < 5; k++) {
            if (playAgain.equalsIgnoreCase("y")) {
                card[k][0] = getRandomNumber();
                card[k][1] = getRandomNumber();

                total = calcTotal(card[k][0], card[k][1]);

                System.out.println("card1: " + card[k][0]);
                System.out.println("card2: " + card[k][1]);

                System.out.println("Do you want another card y/n?");
                choice = keyboard.next();

                for (int i = 2; i < 26; i++) {
                    if (choice.equalsIgnoreCase("y")) {
                        card[k][i] = getRandomNumber();
                        System.out.println("newCard: " + card[k][i]);
                        total = calcTotal(total, card[k][i]);
                        System.out.println("Do you want another card y/n?");
                        choice = keyboard.next();
                    }
                }
                System.out.print("cards drawn: ");
                for (int j = 0; j < 26; j++) {
                    if (card[k][j] > 0) {
                        drawnCards[card[k][j]-1]++;
                        System.out.print(card[k][j] + " ");
                    }
                }

                System.out.println("\ntotal: " + total);

                houseTotal = getHouseNumber();
                System.out.println("house: " + houseTotal);

                if (getResult(total)) {
                    System.out.println("You have won!");
                } else {
                    System.out.println("You have lost...");
                }

                if (busted(total)) {
                    System.out.println("Busted!");
                }
                System.out.println("Would you like to play again? y/n?");
                playAgain = keyboard.next();

            }
        }
        System.out.println("You have won " + percentageOfGamesWon(card) + "% of rounds.");

        System.out.println("Histogram of drawn cards: ");
        for (int l = 0; l < 10; l++)
        {
            System.out.print((l+1) + " ");
            if (drawnCards[l] > 0)
            {
                for(int m = 0; m < drawnCards[l]; m++)
                {
                    System.out.print("*");
                }
            }
            System.out.print("\n");
        }
    }
}
